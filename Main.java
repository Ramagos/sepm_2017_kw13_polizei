import java.beans.*;
import java.io.*;
import java.util.*;

public class Main implements Serializable{

	static ArrayList <PKW> pkws=new ArrayList <PKW>();
	static ArrayList <Motorrad> motorrader=new ArrayList <Motorrad>();
	static ArrayList <Transporter> transporter=new ArrayList <Transporter>();
	
	public static void main(String[] args) {
		loadData();
		
		View handel=new View();
		while(true){
			handel.start();
			saveData();
		}
	}
	
	public static ArrayList<PKW> getPKWs(){
		return pkws;
	}
	public static ArrayList<Motorrad> getMotorrader(){
		return motorrader;
	}
	public static ArrayList<Transporter> getTransporter(){
		return transporter;
	}
	public static void setPkws(ArrayList<PKW> pkws) {
		Main.pkws = pkws;
	}
	public static void setMotorrader(ArrayList<Motorrad> motorrader) {
		Main.motorrader = motorrader;
	}
	public static void setTransporter(ArrayList<Transporter> transporter) {
		Main.transporter = transporter;
	}

	public static void saveData(){
		XMLEncoder enc;
		try{
			enc=new XMLEncoder(new BufferedOutputStream(new FileOutputStream("c:\\temp\\fahrzeuge.xml")));
			enc.writeObject(getPKWs());
			enc.writeObject(getMotorrader());
			enc.writeObject(getTransporter());
			enc.close();
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
	public static void loadData(){
		XMLDecoder dec;
		try{
			dec=new XMLDecoder(new BufferedInputStream(new FileInputStream("c:\\temp\\fahrzeuge.xml")));
			setPkws((ArrayList<PKW>)dec.readObject());
			setMotorrader((ArrayList<Motorrad>)dec.readObject());
			setTransporter((ArrayList<Transporter>)dec.readObject());
			dec.close();
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
}