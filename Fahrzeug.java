import java.io.Serializable;

public class Fahrzeug implements Serializable{
	public int baujahr;
	public String hersteller;
	public String kennzeichen;
	public String fahrzeugtyp;
	public boolean belegt;

	public Fahrzeug(){}
	
	public Fahrzeug(int baujahr, String hersteller, String kennzeichen, String fahrzeugtyp, boolean belegt){
		this.baujahr=baujahr;
		this.hersteller=hersteller;
		this.kennzeichen=kennzeichen;
		this.fahrzeugtyp=fahrzeugtyp;
		this.belegt=belegt;
	}

	public void ausleihen(){
		if(this.belegt==true){
			System.out.println(this.fahrzeugtyp+" schon in Verwendung!");
		}else{
			this.belegt=true;
			System.out.println(this.fahrzeugtyp+" erfolgreich ausgeliehen!");
		}
	}

	public void zurueckgeben(){
		if(this.belegt==true){
			this.belegt=false;
			System.out.println(this.fahrzeugtyp+" erfolgreich zur�ckgegeben!");
		}else{
			System.out.println(this.fahrzeugtyp+" ist noch nicht ausgeliehen worden!");
		}
	}

	public int getBaujahr() {
		return baujahr;
	}

	public String getHersteller() {
		return hersteller;
	}

	public String getKennzeichen() {
		return kennzeichen;
	}

	public String getFahrzeugtyp() {
		return fahrzeugtyp;
	}

	public boolean isBelegt() {
		return belegt;
	}

	public void setBaujahr(int baujahr) {
		this.baujahr = baujahr;
	}

	public void setHersteller(String hersteller) {
		this.hersteller = hersteller;
	}

	public void setKennzeichen(String kennzeichen) {
		this.kennzeichen = kennzeichen;
	}

	public void setFahrzeugtyp(String fahrzeugtyp) {
		this.fahrzeugtyp = fahrzeugtyp;
	}

	public void setBelegt(boolean belegt) {
		this.belegt = belegt;
	}
}