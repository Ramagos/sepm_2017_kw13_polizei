import java.io.Serializable;

public class Transporter extends Fahrzeug implements Serializable{
	public double ladeflaeche;
	public int sitzplaetze;

	public Transporter(){}	
	
	public Transporter(int baujahr, String hersteller, String kennzeichen, double ladeflaeche, int sitzplaetze, boolean belegt){
		super(baujahr, hersteller, kennzeichen, "Transporter", belegt);
		this.ladeflaeche=ladeflaeche;
		this.sitzplaetze=sitzplaetze;
	}

	public double getLadeflaeche() {
		return ladeflaeche;
	}

	public int getSitzplaetze() {
		return sitzplaetze;
	}

	public void setLadeflaeche(double ladeflaeche) {
		this.ladeflaeche = ladeflaeche;
	}

	public void setSitzplaetze(int sitzplaetze) {
		this.sitzplaetze = sitzplaetze;
	}
	
}