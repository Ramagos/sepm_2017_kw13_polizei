import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class View {

	public View(){}

	public void start(){
		System.out.println("Wollen sie ein Auto ausleihen oder zur�ckgeben?");
		System.out.println("(a/z)");
		Scanner sc = new Scanner(System.in);
		String auswahl = sc.next();

		if(auswahl.equals("z")){
			int autoZuruck=-1;
			String autoArtZuruck="x";
			System.out.println("Welches Auto wollen sie zur�ckgeben?");
			System.out.println("Kennzeichen des Autos:");
			String kennzeichen = sc.next();

			for(int i=0;i<Main.getPKWs().size();i++){
				if(Main.getPKWs().get(i).kennzeichen.equals(kennzeichen)){
					autoZuruck=i;
					autoArtZuruck="p";
				}
			}
			if(autoZuruck==-1)
				for(int i=0;i<Main.getMotorrader().size();i++){
					if(Main.getMotorrader().get(i).kennzeichen.equals(kennzeichen)){
						autoZuruck=i;
						autoArtZuruck="m";
					}
				}
			if(autoZuruck==-1)
				for(int i=0;i<Main.getTransporter().size();i++){
					if(Main.getTransporter().get(i).kennzeichen.equals(kennzeichen)){
						autoZuruck=i;
						autoArtZuruck="t";
					}
				}
			if(autoArtZuruck.equals("p"))Main.getPKWs().get(autoZuruck).zurueckgeben();
			if(autoArtZuruck.equals("m"))Main.getMotorrader().get(autoZuruck).zurueckgeben();
			if(autoArtZuruck.equals("t"))Main.getTransporter().get(autoZuruck).zurueckgeben();
		}

		if(auswahl.equals("a")){
			System.out.println("Wollen sie einen PKW, ein Motorrad oder einen Transporter ausleihen?");
			System.out.println("(p/m/t)");
			String pmt = sc.next();
			String klasse;

			int anzahl=0;
			if(pmt.equals("p")){
				for(int i=0;i<Main.getPKWs().size();i++){
					klasse=Main.getPKWs().get(i).getClass()+" ";
					if(!Main.getPKWs().get(i).belegt)
						System.out.println(i+": "
								+klasse.substring(6)
								+Main.getPKWs().get(i).baujahr+" "
								+Main.getPKWs().get(i).hersteller+" "
								+Main.getPKWs().get(i).besonderheiten);
					anzahl=i;
				}
				System.out.println("Welchen der PKWs wollen sie ausleihen?");
				int fz = Integer.parseInt(sc.next());
				Main.getPKWs().get(fz).ausleihen();
			}

			if(pmt.equals("m")){
				for(int i=0;i<Main.getMotorrader().size();i++){
					klasse=Main.getMotorrader().get(i).getClass()+" ";
					if(!Main.getMotorrader().get(i).belegt)
						System.out.println(i+": "
								+klasse.substring(6)
								+Main.getMotorrader().get(i).baujahr+" "
								+Main.getMotorrader().get(i).hersteller+" "
								+Main.getMotorrader().get(i).hubraum);
					anzahl=i;
				}
				System.out.println("Welches der Motorr�der wollen sie ausleihen?");
				int fz = Integer.parseInt(sc.next());
				Main.getMotorrader().get(fz).ausleihen();
			}

			if(pmt.equals("t")){
				for(int i=0;i<Main.getTransporter().size();i++){
					klasse=Main.getTransporter().get(i).getClass()+" ";
					if(!Main.getTransporter().get(i).belegt)
						System.out.println(i+": "
								+klasse.substring(6)
								+Main.getTransporter().get(i).baujahr+" "
								+Main.getTransporter().get(i).hersteller+" "
								+Main.getTransporter().get(i).ladeflaeche+" "
								+Main.getTransporter().get(i).sitzplaetze);
					anzahl=i;
				}
				System.out.println("Welchen der Transporter wollen sie ausleihen?");
				int fz = Integer.parseInt(sc.next());
				Main.getTransporter().get(fz).ausleihen();
			}
		}
	}
}