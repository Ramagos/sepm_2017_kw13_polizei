import java.io.Serializable;

public class Motorrad extends Fahrzeug implements Serializable{
	public int hubraum;

	public Motorrad(){}	
	
	public Motorrad(int baujahr, String hersteller, String kennzeichen, int hubraum, boolean belegt) {
		super(baujahr, hersteller, kennzeichen, "Motorrad", belegt);
		this.hubraum=hubraum;
	}

	public int getHubraum() {
		return hubraum;
	}

	public void setHubraum(int hubraum) {
		this.hubraum = hubraum;
	}
	
}