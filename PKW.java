import java.io.Serializable;

public class PKW extends Fahrzeug implements Serializable{
	public String besonderheiten;
	
	public PKW(){}	
	
	public PKW(int baujahr, String hersteller, String kennzeichen, String besonderheiten, boolean belegt){
		super(baujahr, hersteller, kennzeichen, "PKW", belegt);
		this.besonderheiten=besonderheiten;
	}

	public String getBesonderheiten() {
		return besonderheiten;
	}

	public void setBesonderheiten(String besonderheiten) {
		this.besonderheiten = besonderheiten;
	}
	
}